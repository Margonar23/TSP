
package ch.unused;


import ch.Parser;
import ch.Problem;

/**
 * Created by Install on 12.04.2017.
 */
public class Settings {

    public static void main(String[] args) {
        String path = "src\\trips\\";

        String[] file = new String[10];
        //file[0] = "ch130";
        file[0] = "d198";
        //file[2] = "eil76";
        file[1] = "fl1577";
        //file[4] = "kroA100";
        file[2] = "lin318";
        file[3] = "pcb442";
        file[4] = "pr439";
        file[5] = "rat783";
        file[6] = "u1060";


        double meanError = 0;

        for (int i = 0; i < file.length; i++) {
            double minError = Integer.MAX_VALUE;
            int bestStartCity = -1;
            long bestSeed = -1;
            for (int j = 0; j < 400; j++) {
                if (minError == 0.0) {
                    break;
                }
                long endTime = System.currentTimeMillis() + (3 * 60 * 1000);
                long seed = System.currentTimeMillis();
                Problem problem = Parser.parseTSP(path + file[i] + ".tsp", seed);
                AlgorithmTest algorithm = new AlgorithmTest(problem, endTime);
                int startCity = problem.getRandomStartCity();
                int[] tour = algorithm.nearestNeighbor(startCity);
                tour = algorithm.twoOpt(tour);
                tour = algorithm.simulatedAnnealing(tour);
                double error = algorithm.error(tour);
                if (error < minError) {
                    minError = error;
                    bestSeed = seed;
                    bestStartCity = startCity;
                    meanError += minError;
                }
            }

            System.err.println(file[i] + " -> [" + minError + "] - [" + bestSeed + "] - [" + bestStartCity + "]");
        }

        System.out.println("Mean Error: " + meanError / 10);

    }
}
