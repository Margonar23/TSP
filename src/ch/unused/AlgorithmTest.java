package ch.unused;


import ch.Problem;

import java.util.*;

/**
 * Created by Margonar on 20.03.2017.
 */
public class AlgorithmTest {

    int[] cities;
    ArrayList<Integer> notVisited = new ArrayList<>();
    int[][] distance;
    Problem problem;
    long endTime;

    Random random;

    public AlgorithmTest(Problem problem, long endTime) {
        this.distance = problem.getDistance();
        this.problem = problem;
        this.endTime = endTime;
        this.random = problem.getRandom();
        this.cities = new int[distance.length];
        for (int i = 0; i < distance.length; i++) {
            notVisited.add(i);
        }
    }

    public int[] nearestNeighbor(int position) {
        cities[0] = notVisited.remove(position);

        for (int i = 1; i < distance.length; i++) {
            int min = Integer.MAX_VALUE;
            int index = -1;
            int j = 0;
            for (int city : notVisited) {
                if (distance[cities[i - 1]][city] < min) {
                    min = distance[cities[i - 1]][city];
                    index = j;
                }
                j++;
            }

            cities[i] = notVisited.remove(index);
        }

        return cities;
    }

    public int[] twoOpt(int[] tour) {
        int bestGain;
        int gain;
        int bJ;
        int bI;
        do {
            bestGain = 0;
            bJ = 0;
            bI = 0;
            for (int i = 0; i < tour.length - 2; ++i) {
                for (int j = i + 2; j < tour.length; ++j) {
                    int d0 = distance[tour[i]][tour[j]];
                    int d1 = distance[tour[i + 1]][tour[(j + 1) % tour.length]];

                    int d2 = distance[tour[i]][tour[i + 1]];
                    int d3 = distance[tour[j]][tour[(j + 1) % tour.length]];

                    gain = (d0 + d1) - (d2 + d3);
                    if (gain < bestGain) {

                        bestGain = gain;
                        bJ = j;
                        bI = i + 1;
                    }
                }
            }

            tour = optSwap(tour, bI, bJ);


        } while (bestGain < 0);


        return tour;
    }

    public int[] simulatedAnnealing(int[] tour) {
        double temperature = 120;
        int[] current = tour;
        int[] best = tour;
        int[] next;
        int nextDistance = 0, currentDistance = distance(current), delta = 0;
        int bestDistance = currentDistance;

        while (System.currentTimeMillis() < endTime) {
            for (int i = 0; i < 10; i++) {
                next = randomSwap(current);
                next = twoOpt(next);
                nextDistance = distance(next);
                delta = nextDistance - currentDistance;
                if (delta < 0) {
                    current = next;
                    currentDistance = nextDistance;
                    if (nextDistance < bestDistance) {
                        best = Arrays.copyOf(next, next.length);
                        bestDistance = nextDistance;
                    } else {
                        if (random.nextDouble() < Math.exp(-delta / temperature)) {
                            current = next;
                            currentDistance = nextDistance;
                        }
                    }
                }
            }

            temperature *= 0.98;
        }

        return best;

    }


    public int[] randomSwap(int[] trip) {
//        boolean newRandom = false;
//        List<Integer> pos;
//        do {
//            newRandom = false;
//            pos = new ArrayList<Integer>() {{
//                add((int) (random.nextDouble() * (trip.length - 1)));//a 0
//                add((int) (random.nextDouble() * (trip.length - 1)));//b 1
//                add((int) (random.nextDouble() * (trip.length - 1)));//c 2
//                add((int) (random.nextDouble() * (trip.length - 1)));//d 3
//                add((int) (random.nextDouble() * (trip.length - 1)));//e 4
//            }};
//
//            Collections.sort(pos);
//
//            int tmp = pos.get(0);
//            for (int i = 1; i < pos.size(); i++) {
//
//                if (tmp == pos.get(i) || tmp - 1 == pos.get(i) || tmp + 1 == pos.get(i)) {
//                    newRandom = true;
//                    break;
//                }
//
//                tmp = pos.get(i);
//            }
//        } while (newRandom);
        int size = trip.length;
        ArrayList<Integer> pos = new ArrayList<Integer>(4) {{
            add(random.nextInt(size - 10));
            add(random.nextInt(size - 8 - get(0)) + get(0) + 2);
            add(random.nextInt(size - 6 - get(1)) + get(1) + 2);
            add(random.nextInt(size - 4 - get(2)) + get(2) + 2);

        }};

        int a = pos.get(0);
        int b = pos.get(1);
        int c = pos.get(2);
        int d = pos.get(3);

        int[] solution = new int[trip.length];

        System.arraycopy(trip, 0, solution, 0, a + 1);
        System.arraycopy(trip, c + 1, solution, a + 1, d - c);
        System.arraycopy(trip, b + 1, solution, a + 1 + (d - c), c - b);
        System.arraycopy(trip, a + 1, solution, a + 1 + (d - c) + (c - b), b - a);
        System.arraycopy(trip, d + 1, solution, a + 1 + (d - c) + (c - b) + (b - a), trip.length - 1 - d);

        return solution;
    }

    public int[] optSwap(int[] tour, int i, int j) {

        while (i < j) {
            int tmp = tour[i];
            tour[i] = tour[j];
            tour[j] = tmp;
            i++;
            j--;
        }

        return tour;
    }


    public int distance(int[] tour) {
        int total = 0;

        for (int i = 0; i < tour.length - 1; i++) {
            total += distance[tour[i]][tour[i + 1]];
        }
        total += distance[tour[0]][tour[tour.length - 1]];
        return total;
    }

    public double error(int[] tour) {
        return (double) (distance(tour) - problem.getBestKnow()) / problem.getBestKnow() * 100;
    }

}
