//package ch;
//
//import ch.IO.Parser;
//
//import java.io.IOException;
//import java.util.HashSet;
//import java.util.Set;
//import java.util.TimerTask;
//
///**
// * Created by Margonar on 30.03.2017.
// */
//public class Executor {
//
//    String path = "src\\trips\\";
//    Problem problem;
//    Algorithm algorithm;
//    double meanError = 0;
//    int calls = 0;
//    boolean verbose = false;
//    boolean write = false;
//
//    public double ch130(long seed) {
//        String name = "ch130.tsp";
//        int position = 42;
//        long endTime = System.currentTimeMillis() + (3 * 60 * 1000);
//        problem = Parser.parseTSP(path + name,seed);
//        algorithm = new Algorithm(problem, endTime);
//        int[] tour;
//        tour = algorithm.nearestNeighbor(position);
//        tour = algorithm.twoOpt(tour);
//        tour = algorithm.simulatedAnnealing(tour);
//        printer(position, tour, seed);
//        double error = algorithm.error(tour);
//        meanError += error;
//        if (write){
//            try {
//                Parser.saveTour(seed,error,problem,tour);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//        calls++;
//        return algorithm.error(tour);
//    }
//
//    public double d198(long seed) {
//        String name = "d198.tsp";
//        int position = 46;
//        long endTime = System.currentTimeMillis() + (3 * 60 * 1000);
//        problem = Parser.parseTSP(path + name,seed);
//        algorithm = new Algorithm(problem, endTime);
//        int[] tour;
//        tour = algorithm.nearestNeighbor(position);
//        tour = algorithm.twoOpt(tour);
//        tour = algorithm.simulatedAnnealing(tour);
//        printer(position, tour, seed);
//        double error = algorithm.error(tour);
//        meanError += error;
//        if (write){
//            try {
//                Parser.saveTour(seed,error,problem,tour);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//        calls++;
//        return algorithm.error(tour);
//    }
//
//    public double eil76(long seed) {
//        String name = "eil76.tsp";
//        int position = 48;
//        long endTime = System.currentTimeMillis() + (3 * 60 * 1000);
//        problem = Parser.parseTSP(path + name, seed);
//        algorithm = new Algorithm(problem, endTime);
//        int[] tour;
//        tour = algorithm.nearestNeighbor(position);
//        tour = algorithm.twoOpt(tour);
//        tour = algorithm.simulatedAnnealing(tour);
//        printer(position, tour, seed);
//        double error = algorithm.error(tour);
//        meanError += error;
//        if (write){
//            try {
//                Parser.saveTour(seed,error,problem,tour);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//        calls++;
//        return algorithm.error(tour);
//    }
//
//    public double fl1577(long seed) {
//        String name = "fl1577.tsp";
//        int position = 906;
//        long endTime = System.currentTimeMillis() + (3 * 60 * 1000);
//        problem = Parser.parseTSP(path + name,seed);
//        algorithm = new Algorithm(problem, endTime);
//        int[] tour;
//        tour = algorithm.nearestNeighbor(position);
//        tour = algorithm.twoOpt(tour);
//        tour = algorithm.simulatedAnnealing(tour);
//        printer(position, tour, seed);
//        double error = algorithm.error(tour);
//        meanError += error;
//        if (write){
//            try {
//                Parser.saveTour(seed,error,problem,tour);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//        calls++;
//        return algorithm.error(tour);
//    }
//
//    public double kroA100(long seed) {
//        String name = "kroA100.tsp";
//        int position = 37;
//        long endTime = System.currentTimeMillis() + (3 * 60 * 1000);
//        problem = Parser.parseTSP(path + name,seed);
//        algorithm = new Algorithm(problem, endTime);
//        int[] tour;
//        tour = algorithm.nearestNeighbor(position);
//        tour = algorithm.twoOpt(tour);
//        tour = algorithm.simulatedAnnealing(tour);
//        printer(position, tour, seed);
//        double error = algorithm.error(tour);
//        meanError += error;
//        if (write){
//            try {
//                Parser.saveTour(seed,error,problem,tour);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//        calls++;
//        return algorithm.error(tour);
//    }
//
//    public double lin318(long seed) {
//        String name = "lin318.tsp";
//        int position = 85;
//        long endTime = System.currentTimeMillis() + (3 * 60 * 1000);
//        problem = Parser.parseTSP(path + name,seed);
//        algorithm = new Algorithm(problem, endTime);
//        int[] tour;
//        tour = algorithm.nearestNeighbor(position);
//        tour = algorithm.twoOpt(tour);
//        tour = algorithm.simulatedAnnealing(tour);
//        printer(position, tour, seed);
//        double error = algorithm.error(tour);
//        meanError += error;
//        if (write){
//            try {
//                Parser.saveTour(seed,error,problem,tour);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//        calls++;
//        return algorithm.error(tour);
//    }
//
//    public double pcb442(long seed) {
//        String name = "pcb442.tsp";
//        int position = 98;
//        long endTime = System.currentTimeMillis() + (3 * 60 * 1000);
//        problem = Parser.parseTSP(path + name,seed);
//        algorithm = new Algorithm(problem, endTime);
//        int[] tour;
//        tour = algorithm.nearestNeighbor(position);
//        tour = algorithm.twoOpt(tour);
//        tour = algorithm.simulatedAnnealing(tour);
//        printer(position, tour, seed);
//        double error = algorithm.error(tour);
//        meanError += error;
//        if (write){
//            try {
//                Parser.saveTour(seed,error,problem,tour);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//        calls++;
//        return algorithm.error(tour);
//    }
//
//    public double pr439(long seed) {
//        String name = "pr439.tsp";
//        int position = 21;
//        long endTime = System.currentTimeMillis() + (3 * 60 * 1000);
//        problem = Parser.parseTSP(path + name,seed);
//        algorithm = new Algorithm(problem, endTime);
//        int[] tour;
//        tour = algorithm.nearestNeighbor(position);
//        tour = algorithm.twoOpt(tour);
//        tour = algorithm.simulatedAnnealing(tour);
//        printer(position, tour, seed);
//        double error = algorithm.error(tour);
//        meanError += error;
//        if (write){
//            try {
//                Parser.saveTour(seed,error,problem,tour);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//        calls++;
//        return algorithm.error(tour);
//    }
//
//    public double rat783(long seed) {
//        String name = "rat783.tsp";
//        int position = 272;
//        long endTime = System.currentTimeMillis() + (3 * 60 * 1000);
//        problem = Parser.parseTSP(path + name,seed);
//        algorithm = new Algorithm(problem, endTime);
//        int[] tour;
//        tour = algorithm.nearestNeighbor(position);
//        tour = algorithm.twoOpt(tour);
//        tour = algorithm.simulatedAnnealing(tour);
//        printer(position, tour, seed);
//        double error = algorithm.error(tour);
//        meanError += error;
//        if (write){
//            try {
//                Parser.saveTour(seed,error,problem,tour);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//        calls++;
//        return algorithm.error(tour);
//    }
//
//    public double u1060(long seed) {
//        String name = "u1060.tsp";
//        int position = 939;
//        long endTime = System.currentTimeMillis() + (3 * 60 * 1000);
//        problem = Parser.parseTSP(path + name,seed);
//        algorithm = new Algorithm(problem, endTime);
//        int[] tour;
//        tour = algorithm.nearestNeighbor(position);
//        tour = algorithm.twoOpt(tour);
//        tour = algorithm.simulatedAnnealing(tour);
//        printer(position, tour, seed);
//        double error = algorithm.error(tour);
//        meanError += error;
//        if (write){
//            try {
//                Parser.saveTour(seed,error,problem,tour);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//        calls++;
//        return algorithm.error(tour);
//    }
//
//
//    private void printer(int startCity, int[] trip, long seed) {
//        if (verbose) {
//            System.out.println("--------------------" + problem.getProblemName() + "--------------------");
//            System.out.println("Start city: " + startCity);
//            System.out.println("Best know: " + problem.getBestKnow());
//            System.out.println("My best: " + algorithm.distance(trip));
//            System.out.println("Error: " + algorithm.error(trip));
//            System.out.println("Seed: " + seed);
////            if (checkDuplicates(trip)) {
////                System.out.println("No duplicates");
////            } else {
////                System.out.println("Error: duplicates found");
////            }
//        } else {
//            // scrivo su file i risultati
//        }
//    }
//
//    public void printTrip(int[] trip) {
//        for (int i = 0; i < trip.length; i++) {
//            System.out.print(trip[i] + " | ");
//        }
//        System.out.println();
//    }
//
//    public void meanError() {
//            System.out.println("-------------------------------------------------------------------------");
//            System.out.println("MEAN ERROR: " + meanError / calls);
//            System.out.println("-------------------------------------------------------------------------");
//
//    }
//
//    static boolean checkDuplicates(final int[] trip) {
//        Set<Integer> lump = new HashSet<Integer>();
//        for (int i : trip) {
//            if (lump.contains(i))
//                return false;
//            lump.add(i);
//        }
//        return true;
//    }
//}
//
//
