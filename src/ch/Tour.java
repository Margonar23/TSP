package ch;

/**
 * Created by Install on 19.04.2017.
 */
public class Tour {
    int[] tour;
    int distance;
    Problem problem;
    int[] best;

    public Tour(Problem problem) {
        this.problem = problem;
        setTour(new int[problem.getDimension()]);
        setDistance(Integer.MAX_VALUE);
        best = new int[problem.getDimension()];
    }

    public int[] getBest() {
        return best;
    }

    public void setBest(int[] best) {
        this.best = best;
    }

    public Problem getProblem() {
        return problem;
    }

    public void setProblem(Problem problem) {
        this.problem = problem;
    }

    public int[] getTour() {
        return tour;
    }

    public void setTour(int[] tour) {
        this.tour = tour;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public double getError() {
        return (double) (getDistance() - this.problem.getBestKnow()) / this.problem.getBestKnow() * 100;
    }

}
