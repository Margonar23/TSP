package ch;


import java.util.*;

/**
 * Created by Margonar on 20.03.2017.
 */
public class Algorithm {

    ArrayList<Integer> notVisited = new ArrayList<>();
    int[][] distance;
    Problem problem;
    Random random;
    Tour tour;

    public Algorithm(Problem problem, Tour tour) {
        this.distance = problem.getDistance();
        this.problem = problem;
        this.random = problem.getRandom();
        for (int i = 0; i < distance.length; i++) {
            notVisited.add(i);
        }
        this.tour = tour;
    }

    public void nearestNeighbor(int position) {
        int[] trip = tour.getTour();
        trip[0] = notVisited.remove(position);

        for (int i = 1; i < distance.length; i++) {
            int min = Integer.MAX_VALUE;
            int index = -1;
            int j = 0;
            for (int city : notVisited) {
                if (distance[trip[i - 1]][city] < min) {
                    min = distance[trip[i - 1]][city];
                    index = j;
                }
                j++;
            }
            trip[i] = notVisited.remove(index);
        }

        tour.setTour(trip);
    }

    public void twoOpt() {
        int bestGain;
        int gain;
        int bJ;
        int bI;
        int[] trip = tour.getTour();
        do {
            bestGain = 0;
            bJ = 0;
            bI = 0;
            for (int i = 0; i < trip.length - 2; ++i) {
                for (int j = i + 2; j < trip.length; ++j) {
                    int d0 = distance[trip[i]][trip[j]];
                    int d1 = distance[trip[i + 1]][trip[(j + 1) % trip.length]];
                    int d2 = distance[trip[i]][trip[i + 1]];
                    int d3 = distance[trip[j]][trip[(j + 1) % trip.length]];

                    gain = (d0 + d1) - (d2 + d3);
                    if (gain < bestGain) {
                        bestGain = gain;
                        bJ = j;
                        bI = i + 1;
                    }
                }
            }

            trip = optSwap(trip, bI, bJ);


        } while (bestGain < 0);

        tour.setTour(trip);
    }

    public void simulatedAnnealing() {
        double temperature = 120;
        int[] current = tour.getTour();
        int[] next;
        int nextDistance = 0, currentDistance = distance(current), delta = 0;
        int bestDistance = currentDistance;

        do {
            for (int i = 0; i < 50; i++) {
                doubleBridge(current);
                twoOpt();
                next = Arrays.copyOf(tour.getTour(),tour.getTour().length);
                nextDistance = distance(next);
                delta = nextDistance - currentDistance;
                if (delta < 0) {
                    current = next;
                    currentDistance = nextDistance;
                    if (nextDistance < bestDistance) {
                        bestDistance = nextDistance;
                        tour.setTour(Arrays.copyOf(next, next.length));
                        tour.setBest(Arrays.copyOf(next,next.length));
                        tour.setDistance(bestDistance);
                    } else {
                        if (random.nextDouble() < Math.exp(-delta / temperature)) {
                            current = next;
                            currentDistance = nextDistance;
                        }
                    }
                }
            }

            temperature *= 0.95;
        } while (true);

    }


    public void doubleBridge(int[] trip) {
        int size = trip.length;
        ArrayList<Integer> pos = new ArrayList<Integer>(4) {{
            add(random.nextInt(size - 10));
            add(random.nextInt(size - 8 - get(0)) + get(0) + 2);
            add(random.nextInt(size - 6 - get(1)) + get(1) + 2);
            add(random.nextInt(size - 4 - get(2)) + get(2) + 2);
        }};

        int a = pos.get(0);
        int b = pos.get(1);
        int c = pos.get(2);
        int d = pos.get(3);

        int[] solution = new int[trip.length];

        System.arraycopy(trip, 0, solution, 0, a + 1);
        System.arraycopy(trip, c + 1, solution, a + 1, d - c);
        System.arraycopy(trip, b + 1, solution, a + 1 + (d - c), c - b);
        System.arraycopy(trip, a + 1, solution, a + 1 + (d - c) + (c - b), b - a);
        System.arraycopy(trip, d + 1, solution, a + 1 + (d - c) + (c - b) + (b - a), trip.length - 1 - d);

        tour.setTour(solution);
    }

    public int[] optSwap(int[] tour, int i, int j) {

        while (i < j) {
            int tmp = tour[i];
            tour[i] = tour[j];
            tour[j] = tmp;
            i++;
            j--;
        }

        return tour;
    }


    public int distance(int[] tour) {
        int total = 0;

        for (int i = 0; i < tour.length - 1; i++) {
            total += distance[tour[i]][tour[i + 1]];
        }
        total += distance[tour[0]][tour[tour.length - 1]];
        return total;
    }


}
