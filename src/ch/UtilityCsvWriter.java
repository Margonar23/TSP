package ch;

import ch.Algorithm;
import ch.Problem;

import java.io.*;

/**
 * Created by Margonar on 30.03.2017.
 */
public class UtilityCsvWriter {
    private static File file;
    private static boolean isnew = false;
    private static PrintWriter pw = null;

    public static boolean create() {
        try {
            file = new File("result.csv");
            isnew = file.createNewFile();
            FileWriter fw = new FileWriter(file, true);
            BufferedWriter bw = new BufferedWriter(fw);
            pw = new PrintWriter(bw);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (isnew) {
            StringBuilder sb = new StringBuilder();
            sb.append("Problem name");
            sb.append(',');
            sb.append("Distance");
            sb.append(',');
            sb.append("Error");
            sb.append(',');
            sb.append("Seed");
            sb.append(',');
            sb.append("Start city");
            sb.append('\n');

            pw.append(sb.toString());
        }


        return true;

    }

    public static boolean write(String problemName, int distance, double error, long seed, int startCity) {
        StringBuilder sb = new StringBuilder();
        sb.append(problemName);
        sb.append(',');
        sb.append(distance);
        sb.append(',');
        sb.append(error);
        sb.append(',');
        sb.append(seed);
        sb.append(',');
        sb.append(startCity);
        sb.append('\n');
        pw.append(sb.toString());

        return true;

    }

    public static void close() {
        pw.close();
    }
}
