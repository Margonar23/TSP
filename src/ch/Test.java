package ch;

/**
 * Created by Install on 12.04.2017.
 */
public class Test {
    public static void main(String[] args) {

        String file = args[0];
        long seed = Long.parseLong(args[1]);
        int startCity = Integer.parseInt(args[2]);

        Builder builder = new Builder(seed, file, startCity);
        builder.execute();

    }

}
