package ch;


import java.io.*;

/**
 * Created by Margonar on 08.03.2017.
 */
public class Parser {
    public static Problem parseTSP(String pathFile,long seed) {
        BufferedReader bReader = null;
        String line = "";
        String name = "";
        int dimension = -1;
        int bestknow = -1;
        double[][] cities = null;
        try {
            bReader = new BufferedReader(new FileReader(pathFile));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {


            while (!(line = bReader.readLine()).equals("NODE_COORD_SECTION")) {
                line.trim();
                if (line.startsWith("NAME")) {
                    name = line.split(":")[1].trim();
                }

                if (line.startsWith("DIMENSION")) {
                    dimension = Integer.parseInt(line.split(":")[1].trim());
                }

                if (line.startsWith("BEST_KNOWN")) {
                    bestknow = Integer.parseInt(line.split(":")[1].trim());
                }
            }
            String currentLine = null;
            int i = 0;
            int j = 0;
            cities = new double[dimension][2];

            while (!(currentLine = bReader.readLine()).equals("EOF")) {
                String[] elementString = currentLine.trim().split(" ");
                cities[i][0] = Double.parseDouble(elementString[1].trim());
                cities[i][1] = Double.parseDouble(elementString[2].trim());
                i++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        int[][] distance = new int[dimension][dimension];
        for (int i = 0; i < dimension; i++) {
            for (int j = 0; j < dimension; j++) {

                distance[i][j] = (int) (Math.sqrt((Math.pow((cities[i][0] - cities[j][0]), 2) + Math.pow((cities[i][1] - cities[j][1]), 2))) + 0.5);
                distance[j][i] = (int) (Math.sqrt((Math.pow((cities[i][0] - cities[j][0]), 2) + Math.pow((cities[i][1] - cities[j][1]), 2))) + 0.5);

            }
        }


        return new Problem(name, bestknow, dimension, distance, seed);
    }




}
