package ch;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by Install on 19.04.2017.
 */
public class Builder {

    long seed;
    String file;
    int startCity;
    Tour tour;
    Algorithm algorithm;
    Problem problem;

    Path currentRelativePath = Paths.get("");
    String s = currentRelativePath.toAbsolutePath().toString();
    String path = s + "\\trips\\";

    public Builder(long seed, String file, int startCity) {
        this.seed = seed;
        this.file = file;
        this.startCity = startCity;
        problem = Parser.parseTSP(path + file + ".tsp", seed);
        this.tour = new Tour(problem);
    }

    // ONLY FOR SEED
//    public Builder(String file) {
//        this.file = file;
//        this.seed = System.currentTimeMillis();
//        problem = Parser.parseTSP(path + file + ".tsp", seed);
//        this.tour = new Tour(problem);
//        this.startCity = problem.getRandomStartCity();
//    }

    public void execute() {

        ScheduledThreadPoolExecutor ex = new ScheduledThreadPoolExecutor(1);
        ex.schedule(() -> {
            System.err.println("Time ENDED: " + file);

            try {
                System.out.println(algorithm.distance(tour.getBest()));
                saveTour(seed, tour); // salvataggio del tour.opt.tour
                // scrivo su file i risultati per cercare il seed migliore
//                UtilityCsvWriter.create();
//                UtilityCsvWriter.write(problem.getProblemName(), tour.getDistance(), tour.getError(), this.seed, this.startCity);
//                UtilityCsvWriter.close();
            } catch (Exception e) {
            }
            System.exit(0);

        }, 3, TimeUnit.MINUTES);


        algorithm = new Algorithm(problem, this.tour);
        algorithm.nearestNeighbor(startCity);
        algorithm.twoOpt();
        algorithm.simulatedAnnealing();
    }

    public void saveTour(long seed, Tour tour) throws IOException {
        Path currentRelativePath = Paths.get("");
        String s = currentRelativePath.toAbsolutePath().toString();
        String path = s + "\\trips\\tour\\";
        String comment = "";
        double error = tour.getError();
        if (error == 0.0) {
            comment = "OPTIMUM FOUND: (" + tour.getDistance() + ")";
        } else {
            comment = "RESULT FOUND: (" + tour.getDistance() + ") - OPTIMAL SOLUTION (" + problem.getBestKnow() + ")";
        }
        BufferedWriter outputWriter;
        outputWriter = new BufferedWriter(new FileWriter(path + tour.getProblem().getProblemName() + ".opt.tour"));
        outputWriter.write("NAME : " + tour.getProblem().getProblemName() + ".opt.tour\n");
        outputWriter.write("COMMENT : " + comment + "\n");
        outputWriter.write("ERROR% : " + error + "\n");
        outputWriter.write("SEED : " + seed + "\n");

        outputWriter.write("TYPE : TOUR\n");
        outputWriter.write("DIMENSION : " + String.valueOf(tour.getProblem().getDimension()) + "\n");
        outputWriter.write("TOUR_SECTION\n");
        for (int i : tour.getBest()) {
            outputWriter.write((i + 1) + "\n");
        }
        outputWriter.write("-1\n");
        outputWriter.write("EOF\n");
        outputWriter.flush();
        outputWriter.close();
    }
}
