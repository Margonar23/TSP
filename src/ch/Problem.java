package ch;

import java.util.Random;

/**
 * Created by Margonar on 08.03.2017.
 */
public class Problem {
    private String problemName;
    private int bestKnow;
    private int dimension;
    private int[][] distance;
    private int[] cities;
    private long seed;
    private Random random;

    public Problem(String problemName, int bestKnow, int dimension, int[][] distance, long seed) {
        this.problemName = problemName;
        this.bestKnow = bestKnow;
        this.dimension = dimension;
        this.distance = distance;
        this.cities = new int[dimension];
        this.random = new Random(seed);
    }


    public int[][] getDistance() {
        return distance;
    }

    public int[] getCities() {
        return cities;
    }

    public String getProblemName() {
        return problemName;
    }

    public int getBestKnow() {
        return bestKnow;
    }

    public int getDimension() {
        return dimension;
    }

    public long getSeed() {
        return seed;
    }

    public void setSeed(long seed) {
        this.seed = seed;
    }

    public Random getRandom() {
        return random;
    }

    public void setRandom(Random random) {
        this.random = random;
    }

    public int getRandomStartCity() {
        Random r = new Random();
        return r.nextInt(this.getDimension());
    }
}
