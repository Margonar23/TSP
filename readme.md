<h1>TSP - Travel Salesman Problem </h1>
<br>
<i>Risolutore Java del problema del commesso viaggiatore.</i>
<br><br>
<ul>
    <li>Legge i problemi nella cartella /trips</li>
    <li>Scrive i risultati nella cartelle /trips/tour</li>
</ul>

<br>
<h3>Algoritmi utilizzati</h3>
<ul>
    <li>Building : Nearest Neighbour</li>
    <li>Local-Optimization : 2opt</li>
    <li>MetaHeuristic : Simulated Annealing </li>
</ul>

<h3>Risultati</h3>
<p>Errore medio: <span style="color: green">0.71%</span></p>
<h5>Singoli problemi</h5>
<table>
<tr><th>Problem</th><th>Seed</th><th>Start City</th><th>Error</th></tr>
<tr><th>ch130</th><td>1492848596201</td><td>44</td><td>0.0%</td></tr>
<tr><th>d198</th><td>1492868665566</td><td>34</td><td>0.0%</td></tr>
<tr><th>eil76</th><td>1492848956619</td><td>75</td><td>0.0%</td></tr>
<tr><th>kroA100</th><td>1492849136808</td><td>1</td><td>0.0%</td></tr>
<tr><th>pcb442</th><td>1493295696442</td><td>176</td><td>0.48%</td></tr>
<tr><th>fl1577</th><td>1493633943509</td><td>774</td><td>1.30%</td></tr>
<tr><th>lin318</th><td>1493129218456</td><td>91</td><td>0.0%</td></tr>
<tr><th>pr439</th><td>1493143641290</td><td>102</td><td>0.02%</td></tr>
<tr><th>rat783</th><td>1493519450563</td><td>248</td><td>2.41%</td></tr>
<tr><th>u1060</th><td>1493079096697</td><td>173</td><td>2.88%</td></tr>
</table>
